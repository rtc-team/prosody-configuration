Debian XMPP service configuration
=================================

This repository holds the configuration for Debian's XMPP service.

Workflow
--------

- Prosody configuration is kept in [salsa](https://salsa.debian.org/rtc-team/prosody-configuration)
- Changes to the configuration are proposed as MR
- MRs have to be backed by a BTS ticket
- Someone else from the team has to approve and merge the MR
- Someone belonging to the `debvoip` ldap group has to ssh into vogler.debian.org and clone this repository
- After cloning the repository, run `make deploy` to deploy the changes 


Limitations
-----------

This repository does not contain all that is required to run the service, help from DSA
is required.

When a second instance of prosody is deployed (#803120, #803122) the rest of the setup will
be added to DSA puppet repository.
