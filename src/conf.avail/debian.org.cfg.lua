VirtualHost "debian.org"
    ssl = {
        key = "/etc/ssl/private/debian.org.key";
        certificate = "/etc/ssl/debian/certs/debian.org.crt-chained";
        }

------ Components ------
-- You can specify components to add hosts that provide special services,
-- like multi-user conferences, and transports.
-- For more information on components, see http://prosody.im/doc/components

---Set up a MUC (multi-user chat):
Component "conference.debian.org" "muc"

